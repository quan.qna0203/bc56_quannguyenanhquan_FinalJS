var arrNhanVien = [];

function makeid(length) {
  let result = "";
  const characters =
    "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
  const charactersLength = characters.length;
  let counter = 0;
  while (counter < length) {
    result += characters.charAt(Math.floor(Math.random() * charactersLength));
    counter += 1;
  }
  return result;
}

document.querySelector("#btnThemNV").onclick = function (e) {
  e.preventDefault();
  var nhanVienNew = new NhanVien();
  console.log(nhanVienNew)
  nhanVienNew.maNhanVien = makeid(10);
  nhanVienNew.tkNV = document.querySelector("#tknv").value;
  nhanVienNew.tenNhanVien = document.querySelector("#name").value;
  nhanVienNew.email = document.querySelector("#email").value;
  nhanVienNew.matKhau = document.querySelector("#password").value;
  nhanVienNew.ngayLam = document.querySelector("#datepicker").value;
  nhanVienNew.luongCoBan = document.querySelector("#luongCB").value;
  nhanVienNew.chucVu = document.querySelector("#chucvu").value;
  nhanVienNew.gioLam = document.querySelector("#gioLam").value;
  console.log(nhanVienNew)
  var valid =
    validation.kiemTraRong(nhanVienNew.tkNV, "tbTKNV", "Tài khoản") &
    validation.kiemTraDoDai(
      nhanVienNew.tkNV,
      "tbTKNVLength",
      "Độ dài tài khoản",
      4,
      6
    ) &
    validation.kiemTraRong(nhanVienNew.tenNhanVien, "tbTen", "Tên nhân viên") &
    validation.kiemTraTatCaKyTu(
      nhanVienNew.tenNhanVien,
      "tbTenDinhDang",
      "Tài khoản"
    ) &
    validation.kiemTraRong(nhanVienNew.email, "tbEmail", "Email") &
    validation.kiemTraEmail(nhanVienNew.email, "tbEmailDinhDang") &
    validation.kiemTraMatKhau(nhanVienNew.matKhau, "tbMatKhauDinhDang") &
    validation.kiemTraRong(nhanVienNew.matKhau, "tbMatKhau", "Mật khẩu") &
    validation.kiemTraNgayThang(nhanVienNew.ngayLam, "tbNgayDinhDang") &
    validation.kiemTraRong(nhanVienNew.ngayLam, "tbNgay", "Ngày làm") &
    validation.kiemTraGiaTri(
      nhanVienNew.luongCoBan,
      "tbLuongCBDinhDang",
      10000000,
      20000000,
      "Lương cở bản thấp nhất là từ 10.000.000 VNĐ và tối đa là 20.000.000 VNĐ"
    ) &
    validation.kiemTraRong(
      nhanVienNew.luongCoBan,
      "tbLuongCB",
      "Lương cơ bản"
    ) &
    validation.kiemTraRong(nhanVienNew.chucVu, "tbChucVu", "Chức vụ") &
    validation.kiemTraRong(nhanVienNew.gioLam, "tbGiolam", "Giờ làm") &
    validation.kiemTraGiaTri(
      nhanVienNew.gioLam,
      "tbGiolamDinhDang",
      80,
      200,
      "Số giờ làm phải từ 80 đến 200 giờ"
    );
  if (!valid) {
    return;
  }
  if (nhanVienNew.chucVu === "sep") {
    nhanVienNew.tongLuong = Number(nhanVienNew.luongCoBan) * 3;
  } else if (this.chucVu === "truongPhong") {
    nhanVienNew.tongLuong = Number(nhanVienNew.luongCoBan) * 2;
  } else {
    nhanVienNew.tongLuong = Number(nhanVienNew.luongCoBan);
  }
  if (nhanVienNew.chucVu === "nhanVien") {
    if (Number(nhanVienNew.gioLam) >= 192) {
      nhanVienNew.loaiNhanVien = "Nhân viên xuất sắc";
    } else if (Number(nhanVienNew.gioLam) >= 176) {
      console.log("gioi");
      nhanVienNew.loaiNhanVien = "Nhân viên giỏi";
    } else if (Number(nhanVienNew.gioLam) >= 160) {
      nhanVienNew.loaiNhanVien = "Nhân viên khá";
    } else if (160 > Number(nhanVienNew.gioLam)) {
      nhanVienNew.loaiNhanVien = "Nhân viên trung bình";
    }
  } else {
    nhanVienNew.loaiNhanVien = "Không xếp loại";
  }

  arrNhanVien.push(nhanVienNew);
  renderTableSV(arrNhanVien);
  saveStorage();

  // validation.kiemTraRong(sinhVien.tenSinhVien, "tenSinhVien");
};
document.querySelector("#btnThem").onclick = function () {
  document.getElementById("form-modal").reset();
};
function renderTableSV(arrNV) {
  outputHTML = "";
  for (var i = 0; i < arrNV.length; i++) {
    var nhanVien = arrNV[i];

    outputHTML += `
            <tr>
                <td>${nhanVien.tkNV}</td>
                <td>${nhanVien.tenNhanVien}</td>
                <td>${nhanVien.email}</td>
                <td>${nhanVien.ngayLam}</td>
                
                <td>${nhanVien.chucVu === "sep"
        ? "Sếp"
        : nhanVien.chucVu === "truongPhong"
          ? "Trưởng phòng"
          : "Nhân viên"
      }</td>
                <td>${VND.format(nhanVien.tongLuong)}</td>
                <td>${nhanVien.loaiNhanVien}</td>
                <td style="width:200px"><button class="btn btn-primary" id="btnCapNhat" onclick="suaNV('${i}')" data-toggle="modal"
                data-target="#myModal">Cập nhật</button>
                <button class="btn btn-danger" id="btnXoa" onclick="xoaNV('${i}')" >Xoá </button>
                </td>
                
            </tr>
        `;
    document.querySelector("#tableDanhSach").innerHTML = outputHTML;
  }
}

function saveStorage() {
  var stringArrNhanVien = JSON.stringify(arrNhanVien);
  localStorage.setItem("arrNhanVien", stringArrNhanVien);
}

function getStorage() {
  if (localStorage.getItem("arrNhanVien")) {
    var stringResult = localStorage.getItem("arrNhanVien");
    // console.log((stringResult))
    arrNhanVien = JSON.parse(stringResult);
  }
}

window.onload = function () {
  getStorage();
  renderTableSV(arrNhanVien);
};

document.querySelector("#searchName").oninput = function (e) {
  var tukhoa = e.target.value;

  var arrResult = [];
  for (var i = 0; i < arrNhanVien.length; i++) {
    var loaiNhanVien = arrNhanVien[i].loaiNhanVien;
    loaiNhanVien = stringToSlug(loaiNhanVien);

    tukhoa = stringToSlug(tukhoa);

    if (loaiNhanVien.search(tukhoa) !== -1) {
      arrResult.push(arrNhanVien[i]);
    }
  }
  renderTableSV(arrResult);
};

function suaNV(index) {
  var nhanVienSua = arrNhanVien[index];

  document.querySelector('#maNhanVien').value = nhanVienSua.maNhanVien;
  document.querySelector("#tknv").value = nhanVienSua.tkNV;
  document.querySelector("#name").value = nhanVienSua.tenNhanVien;
  document.querySelector("#email").value = nhanVienSua.email;
  document.querySelector("#password").value = nhanVienSua.matKhau;
  document.querySelector("#datepicker").value = nhanVienSua.ngayLam;
  document.querySelector("#luongCB").value = nhanVienSua.luongCoBan;
  document.querySelector("#chucvu").value = nhanVienSua.chucVu;
  document.querySelector("#gioLam").value = nhanVienSua.gioLam;
  console.log(nhanVienSua)
}

document.querySelector('#btnCapNhat').onclick = function () {
  var nhanVienUpdate = new NhanVien();

  nhanVienUpdate.maNhanVien = document.querySelector('#maNhanVien').value;
  nhanVienUpdate.tkNV = document.querySelector("#tknv").value;
  nhanVienUpdate.tenNhanVien = document.querySelector("#name").value;
  nhanVienUpdate.email = document.querySelector("#email").value;
  nhanVienUpdate.matKhau = document.querySelector("#password").value;
  nhanVienUpdate.ngayLam = document.querySelector("#datepicker").value;
  nhanVienUpdate.luongCoBan = document.querySelector("#luongCB").value;
  nhanVienUpdate.chucVu = document.querySelector("#chucvu").value;
  nhanVienUpdate.gioLam = document.querySelector("#gioLam").value;

  var valid =
    validation.kiemTraRong(nhanVienUpdate.tkNV, "tbTKNV", "Tài khoản") &
    validation.kiemTraDoDai(
      nhanVienUpdate.tkNV,
      "tbTKNVLength",
      "Độ dài tài khoản",
      4,
      6
    ) &
    validation.kiemTraRong(nhanVienUpdate.tenNhanVien, "tbTen", "Tên nhân viên") &
    validation.kiemTraTatCaKyTu(
      nhanVienUpdate.tenNhanVien,
      "tbTenDinhDang",
      "Tài khoản"
    ) &
    validation.kiemTraRong(nhanVienUpdate.email, "tbEmail", "Email") &
    validation.kiemTraEmail(nhanVienUpdate.email, "tbEmailDinhDang") &
    validation.kiemTraMatKhau(nhanVienUpdate.matKhau, "tbMatKhauDinhDang") &
    validation.kiemTraRong(nhanVienUpdate.matKhau, "tbMatKhau", "Mật khẩu") &
    validation.kiemTraNgayThang(nhanVienUpdate.ngayLam, "tbNgayDinhDang") &
    validation.kiemTraRong(nhanVienUpdate.ngayLam, "tbNgay", "Ngày làm") &
    validation.kiemTraGiaTri(
      nhanVienUpdate.luongCoBan,
      "tbLuongCBDinhDang",
      10000000,
      20000000,
      "Lương cở bản thấp nhất là từ 10.000.000 VNĐ và tối đa là 20.000.000 VNĐ"
    ) &
    validation.kiemTraRong(
      nhanVienUpdate.luongCoBan,
      "tbLuongCB",
      "Lương cơ bản"
    ) &
    validation.kiemTraRong(nhanVienUpdate.chucVu, "tbChucVu", "Chức vụ") &
    validation.kiemTraRong(nhanVienUpdate.gioLam, "tbGiolam", "Giờ làm") &
    validation.kiemTraGiaTri(
      nhanVienUpdate.gioLam,
      "tbGiolamDinhDang",
      80,
      200,
      "Số giờ làm phải từ 80 đến 200 giờ"
    );
  if (!valid) {
    return;
  }

  for (var i = 0; i < arrNhanVien.length; i++) {
    if (arrNhanVien[i].maNhanVien === nhanVienUpdate.maNhanVien) {

      arrNhanVien[i].tkNV = nhanVienUpdate.tkNV;
      arrNhanVien[i].tenNhanVien = nhanVienUpdate.tenNhanVien;
      arrNhanVien[i].email = nhanVienUpdate.email;
      arrNhanVien[i].matKhau = nhanVienUpdate.matKhau;
      arrNhanVien[i].chucVu = nhanVienUpdate.chucVu;
      arrNhanVien[i].gioLam = nhanVienUpdate.gioLam;
      arrNhanVien[i].luongCoBan = nhanVienUpdate.luongCoBan;
      if (arrNhanVien[i].chucVu === "sep") {
        arrNhanVien[i].tongLuong = Number(arrNhanVien[i].luongCoBan) * 3;
      } else if (arrNhanVien[i].chucVu === "truongPhong") {
        arrNhanVien[i].tongLuong = Number(arrNhanVien[i].luongCoBan) * 2;
      } else {

        arrNhanVien[i].tongLuong = Number(arrNhanVien[i].luongCoBan);
      }
      if (arrNhanVien[i].chucVu === "nhanVien") {
        if (Number(arrNhanVien[i].gioLam) >= 192) {
          arrNhanVien[i].loaiNhanVien = "Nhân viên xuất sắc";
        } else if (Number(arrNhanVien[i].gioLam) >= 176) {
          console.log("gioi");
          arrNhanVien[i].loaiNhanVien = "Nhân viên giỏi";
        } else if (Number(arrNhanVien[i].gioLam) >= 160) {
          arrNhanVien[i].loaiNhanVien = "Nhân viên khá";
        } else if (160 > Number(arrNhanVien[i].gioLam)) {
          arrNhanVien[i].loaiNhanVien = "Nhân viên trung bình";
        }
      } else {
        arrNhanVien[i].loaiNhanVien = "Không xếp loại";
      }
      

      break;
    }
  }
  console.log(arrNhanVien)
  renderTableSV(arrNhanVien)
  saveStorage()

}

function xoaNV(index) {
  arrNhanVien.splice(index,1)

  renderTableSV(arrNhanVien)
  saveStorage()
}
